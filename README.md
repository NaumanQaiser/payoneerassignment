# README #

### What is this repository for? ###
* Payoneer Home assignment

### MVVM Architecture - Clean Architecture - ViewBiinding - Retrofit - Glide - Hilt-Dependency Injection - Robolectric - Snackbar ###

**Application Flow

Use the URL to load the payment methods using a GET request in your app.

**Models  
- PaymentResponse

**ViewModel 
- PaymentCheckoutViewModel

**View 
- MainActivity
- PaymentCheckoutFragment

**Hilt 
- App is an hilt application with entry point as MainActivity which injects ViewModel

**XMLs 
- ConstrainLayout is used in payment_checkout_fragment because of its better performance and its adoptability for different screen sizes


### Tests - Robolectric ###*
Few tests written for MainActivity class using robolectric Framework

### Todo's ###
* Remaining Tests