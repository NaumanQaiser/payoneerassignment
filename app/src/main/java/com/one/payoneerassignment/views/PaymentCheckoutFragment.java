package com.one.payoneerassignment.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;
import com.one.payoneerassignment.databinding.PaymentCheckoutFragmentBinding;
import com.one.payoneerassignment.interfaces.OnNetworkItemClicked;
import com.one.payoneerassignment.models.Applicable;
import com.one.payoneerassignment.viewmodels.PaymentCheckoutViewModel;
import com.one.payoneerassignment.views.Adapter.PaymentsAdapter;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class PaymentCheckoutFragment extends Fragment implements OnNetworkItemClicked {

    private PaymentCheckoutViewModel mViewModel;
    private PaymentCheckoutFragmentBinding binding;
    private PaymentsAdapter paymentsAdapter;

    public static PaymentCheckoutFragment newInstance() {
        return new PaymentCheckoutFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = PaymentCheckoutFragmentBinding.inflate(getLayoutInflater());

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialiseViewModel();
    }

    private void initialiseViewModel() {
        mViewModel = new ViewModelProvider(this).get(PaymentCheckoutViewModel.class);
        observeViewViewModel();
        getNetworks();
    }

    private void observeViewViewModel() {
        mViewModel.paymentResponse.observe(this, paymentResponse -> {

            List<Applicable> applicableList = paymentResponse.getNetworks().getApplicable();

            if (!applicableList.isEmpty()) {
                paymentsAdapter = new PaymentsAdapter(this);
                binding.paymentsRecycler.setAdapter(paymentsAdapter);
                paymentsAdapter.submitList(applicableList);
            }
        });

        mViewModel.errorBody.observe(this, errorHolder -> {
            showErrorSnackbar(errorHolder.getMessage());
        });

        mViewModel.isLoading.observe(this, isLoading -> {
            if (isLoading) {
                binding.loadingProgress.setVisibility(View.VISIBLE);
            } else {
                binding.loadingProgress.setVisibility(View.GONE);
            }
        });
    }

    public void getNetworks() {
        mViewModel.getPayments();
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    void showErrorSnackbar(String message) {
        Snackbar snackbar = Snackbar.make(binding.getRoot(), message, Snackbar.LENGTH_INDEFINITE);

        snackbar.show();
        snackbar.setAction("RETRY", v -> {
            snackbar.dismiss();
            getNetworks();
        });
    }

    @Override
    public void getNetwork(Applicable applicable) {

    }
}