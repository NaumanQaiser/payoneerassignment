package com.one.payoneerassignment.views.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.one.payoneerassignment.R;
import com.one.payoneerassignment.databinding.PaymentItemBinding;
import com.one.payoneerassignment.models.Applicable;
import com.one.payoneerassignment.interfaces.OnNetworkItemClicked;

import org.jetbrains.annotations.NotNull;

public class PaymentsAdapter extends ListAdapter<Applicable, PaymentsAdapter.PaymentsViewHolder> {

    private final OnNetworkItemClicked onNetworkItemClicked;


    public PaymentsAdapter(OnNetworkItemClicked onNetworkItemClicked) {
        super(new DiffUtil.ItemCallback<Applicable>() {
            @Override
            public boolean areItemsTheSame(@NonNull Applicable oldItem, @NonNull Applicable newItem) {
                return oldItem.getCode().equals(newItem.getCode());
            }

            @Override
            public boolean areContentsTheSame(@NonNull Applicable oldItem, @NonNull Applicable newItem) {
                return oldItem.getLabel().equals(newItem.getLabel());
            }
        });
        this.onNetworkItemClicked = onNetworkItemClicked;
    }


    @NonNull
    @Override
    public PaymentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PaymentItemBinding binding = PaymentItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PaymentsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentsViewHolder holder, int position) {
        Applicable applicable = getItem(position);
        PaymentItemBinding binding = holder.binding;

        binding.tvPaymentName.setText(applicable.getLabel());

        Glide.with(binding.getRoot().getContext())
                .load(applicable.getLinks().getLogo())
                .placeholder(R.drawable.ic_arrow_back)
                .into(binding.imgNetworkSrc);

        binding.getRoot().setOnClickListener(v -> onNetworkItemClicked.getNetwork(applicable));

    }

    static class PaymentsViewHolder extends RecyclerView.ViewHolder {
        PaymentItemBinding binding;

        public PaymentsViewHolder(PaymentItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}




