package com.one.payoneerassignment.apiservices;

import com.one.payoneerassignment.models.PaymentResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    @GET("develop/shared-test/lists/listresult.json")
    Call<PaymentResponse> getPayments();
}
