package com.one.payoneerassignment;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class PayoneerApp extends Application {
}
