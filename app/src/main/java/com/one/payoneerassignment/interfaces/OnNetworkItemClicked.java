package com.one.payoneerassignment.interfaces;


import com.one.payoneerassignment.models.Applicable;

public interface OnNetworkItemClicked {
    void getNetwork(Applicable applicable);
}