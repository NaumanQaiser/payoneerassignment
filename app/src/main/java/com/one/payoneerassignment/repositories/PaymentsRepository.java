package com.one.payoneerassignment.repositories;


import com.one.payoneerassignment.apiservices.ApiService;
import com.one.payoneerassignment.models.PaymentResponse;

import javax.inject.Inject;

import retrofit2.Call;

public class PaymentsRepository {
    private final ApiService apiService;

    @Inject
    public PaymentsRepository(ApiService apiService) {
        this.apiService = apiService;
    }

    public Call<PaymentResponse> getPaymentNetworks() {
        return apiService.getPayments();
    }
}
